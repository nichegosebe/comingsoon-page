$(document).ready(function() {

    $('.js-form [name="phone"]').mask("+7 999 999-99-99", {autoclear:false});

    $('.js-form [name="phone"]').on('blur', function () {
        var el = $(this);
        var gmdInputText = el.parent('.gmd-input-text');
        if (gmdInputText.hasClass('invalid')) {
            if (isPhone(el.val())) {
                gmdInputText.removeClass('invalid');
            }
        }
    });



    $('.js-form').on('submit', function (e) {
        e.preventDefault();
        var parentEl = $(this).parent(".request");
        var phone = $(this).find('[name="phone"]').val();
        if (!isPhone(phone)) {
            $(this).find('.gmd-input-text').addClass('invalid');
            e.stopPropagation();
            return;
        }
        submitForm(this);
        $(this).addClass('request-form_hide');
    });

    $('.js-request-result-error').on('click', function (e) {
        $(e.target).removeClass('request-result_show');
        $(e.target).parent('.request').find('form').removeClass('hide');
        $(e.target).parent('.request').find('form').removeClass('request-form_hide');
    });

    function isPhone(phone) {
        phone = phone.replace(/[^\d;]/g, '');
        return phone.length == 11;
    }

    function submitForm(form) {
        form = $(form);
        if (form.hasClass('hide')) {
            return;
        }
        var params = form.serialize();
        var url = "/request.php?" + params;

        var request = $.ajax({
            url: url,
            type: "GET",
        });
        request.done(function(result){
            try {
                result = JSON.parse(result);
            } catch(e) {
                result = {success: false};
            }
            if (!result.success) {
                result = {'success': false};
            }
            submitCallback(form, result);
        });
        request.fail(function () {
            var result = {'success':false};
            submitCallback(form, result);
        })
    }

    function submitCallback(form, result) {
        var parentEl = form.parent(".request");
        if (result['success']) {
            parentEl.find('.js-request-result-success').addClass('request-result_show');
        } else {
            parentEl.find('.js-request-result-error').addClass('request-result_show');
        }
        setTimeout(function () {
            $('.js-form').addClass('hide');
        },300)
    }


    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.querySelector(id);
        var daysSpan = clock.querySelector('.js-day');
        var hoursSpan = clock.querySelector('.js-hour');
        var minutesSpan = clock.querySelector('.js-minute');
        var secondsSpan = clock.querySelector('.js-second');
        var daysUnitSpan = clock.querySelector('.js-day-unit');
        var hoursUnitSpan = clock.querySelector('.js-hour-unit');
        var minutesUnitSpan = clock.querySelector('.js-minute-unit');
        var secondsUnitSpan = clock.querySelector('.js-second-unit');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            daysUnitSpan.innerHTML = getNoun(t.days, 'день', 'дня', 'дней');
            hoursUnitSpan.innerHTML = getNoun(t.hours, 'час', 'часа', 'часов');
            minutesUnitSpan.innerHTML = getNoun(t.minutes, 'минута', 'минуты', 'минут');
            secondsUnitSpan.innerHTML = getNoun(t.seconds, 'секунда', 'секунды', 'секунд');

            if (t.total <= 0) {
                clearInterval(timeinterval);
                $('.js-counter').hide();
                $('.js-sales-start-at').hide();
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);

        function getNoun (number, one, few, many) {
            var result = '';

            if ( (number % 100) >= 5 && (number % 100) <= 20) {
                result = many
            } else {
                switch (number % 10) {
                    case 1:
                        result = one;
                        break;
                    case 2:
                    case 3:
                    case 4:
                        result = few;
                        break;
                    default:
                        result = many;
                }
            }
            return result;
        }
    }

    var deadline = new Date(2017,0,10,9,0,0);
    initializeClock('.js-counter', deadline);

    $('.js-show-map').on('click', function () {
        $('#map').css('left', '0%');
        $('.close-map').css('display', 'block');
    });

    $('.close-map').on('click', function () {
        $('#map').css('left', '100%');
        $('.close-map').css('display', 'none');
    });


    setTimeout(init, 6000);

    var map;

    function init(){
        $('#map').html('');
        map = new ymaps.Map("map", {
            center: [45.030160, 38.979517],
            zoom: 18,
            controls: []
        });

        var myPlacemark = new ymaps.Placemark(map.getCenter(), {
            hintContent: 'Собственный значок метки',
            balloonContent: 'Это красивая метка'
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/img/pin.png',
            iconImageSize: [52, 65],
            iconImageOffset: [-26, -65]
        });

        map.geoObjects.add(myPlacemark);

    }
});
